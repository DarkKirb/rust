use crate::cell::UnsafeCell;
use crate::mem;

pub struct Mutex {
    inner: UnsafeCell<ctru_sys::LightLock>,
}

#[inline]
pub unsafe fn raw(m: &Mutex) -> *mut ctru_sys::LightLock {
    m.inner.get()
}

unsafe impl Send for Mutex {}
unsafe impl Sync for Mutex {}

impl Mutex {
    pub const fn new() -> Mutex {
        Mutex { inner: UnsafeCell::new(0) }
    }

    #[inline]
    pub unsafe fn init(&mut self) {
        ctru_sys::LightLock_Init(self.inner.get());
    }

    #[inline]
    pub unsafe fn lock(&self) {
        ctru_sys::LightLock_Lock(self.inner.get());
    }

    #[inline]
    pub unsafe fn unlock(&self) {
        ctru_sys::LightLock_Unlock(self.inner.get());
    }

    #[inline]
    pub unsafe fn try_lock(&self) -> bool {
        match ctru_sys::LightLock_TryLock(self.inner.get()) {
            0 => false,
            _ => true,
        }
    }

    #[inline]
    pub unsafe fn destroy(&self) {
    }
}

pub struct ReentrantMutex { inner: UnsafeCell<ctru_sys::RecursiveLock> }

unsafe impl Send for ReentrantMutex {}
unsafe impl Sync for ReentrantMutex {}

impl ReentrantMutex {
    pub unsafe fn uninitialized() -> ReentrantMutex {
        ReentrantMutex { inner: mem::uninitialized() }
    }

    pub unsafe fn init(&mut self) {
        ctru_sys::RecursiveLock_Init(self.inner.get());
    }

    pub unsafe fn lock(&self) {
        ctru_sys::RecursiveLock_Lock(self.inner.get());
    }

    #[inline]
    pub unsafe fn try_lock(&self) -> bool {
        match ctru_sys::RecursiveLock_TryLock(self.inner.get()) {
            0 => false,
            _ => true,
        }
    }

    pub unsafe fn unlock(&self) {
        ctru_sys::RecursiveLock_Unlock(self.inner.get());
    }

    pub unsafe fn destroy(&self) {}
}
