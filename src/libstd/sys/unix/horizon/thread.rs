use crate::boxed::FnBox;
use crate::cmp;
use crate::ffi::CStr;
use crate::io;
use crate::mem;
use crate::ptr;
use crate::sys::os;
use crate::time::Duration;

use crate::sys_common::thread::*;
use ctru_sys::Thread as ThreadHandle;

pub const DEFAULT_MIN_STACK_SIZE: usize = 4096;

pub struct Thread {
    handle: ThreadHandle,
}
unsafe impl Send for Thread {}
unsafe impl Sync for Thread {}

impl Thread {
    pub unsafe fn new(stack: usize, p: Box<dyn FnBox()>) -> io::Result<Thread> {
        let p = box p;
        let stack_size = cmp::max(stack, DEFAULT_MIN_STACK_SIZE);

        let mut priority = 0;
        ctru_sys::svcGetThreadPriority(&mut priority, 0xFFFF8000);

        let handle = ctru_sys::threadCreate(Some(thread_func), &*p as *const _ as *mut _,
                                             stack_size, priority, -2, false);

        return if handle == ptr::null_mut() {
            Err(io::Error::from_raw_os_error(libc::EAGAIN))
        } else {
            mem::forget(p); // ownership passed to the new thread
            Ok(Thread { handle: handle })
        };

        extern "C" fn thread_func(start: *mut libc::c_void) {
            unsafe { start_thread(start as *mut u8) }
        }
    }
    
    pub fn yield_now() {
        unsafe {
            ctru_sys::svcSleepThread(0)
        }
    }

    pub fn set_name(_name: &CStr) {
        // threads aren't named in libctru
    }

    pub fn sleep(dur: Duration) {
        unsafe {
            let nanos = dur.as_secs()
                .saturating_mul(1_000_000_000)
                .saturating_add(dur.subsec_nanos() as u64);
            ctru_sys::svcSleepThread(nanos as i64)
        }
    }

    pub fn join(self) {
        unsafe {
            let ret = ctru_sys::threadJoin(self.handle, u64::max_value());
            ctru_sys::threadFree(self.handle);
            mem::forget(self);
            debug_assert_eq!(ret, 0);
        }
    }

    #[allow(dead_code)]    
    pub fn id(&self) -> ThreadHandle {
        self.handle
    }

    #[allow(dead_code)]
    pub fn into_id(self) -> ThreadHandle {
        let handle = self.handle;
        mem::forget(self);
        handle
    }
}

impl Drop for Thread {
    fn drop(&mut self) {
        unsafe { ctru_sys::threadDetach(self.handle) }
    }
}

pub mod guard {
    use crate::ops::Range;
    pub type Guard = Range<usize>;
    pub unsafe fn current() -> Option<Guard> { None }
    pub unsafe fn init() -> Option<Guard> { None }
}
