use crate::io::{self, Error, ErrorKind};
use crate::ptr;
use crate::sys::cvt;
use crate::sys::process::process_common::*;
use crate::sys;
use crate::sys::unsupported;

use libc::{c_int, gid_t, pid_t, uid_t};

impl Command {
    pub fn spawn(&mut self, default: Stdio, needs_stdin: bool)
                 -> io::Result<(Process, StdioPipes)> {
        unsupported()
    }
    pub fn exec(&mut self, default: Stdio) -> io::Error {
        if let Err(e) = unsupported::<()>() {
            e
        } else {
            panic!("shouldn't happen");
        }
    }
}

pub struct Process {
    pid: pid_t,
    status: Option<ExitStatus>
}

impl Process {
    pub fn id(&self) -> u32 {
        self.pid as u32
    }
    pub fn kill(&mut self) -> io::Result<()> {
        unsupported()
    }
    pub fn wait(&mut self) -> io::Result<ExitStatus> {
        unsupported()
    }
    pub fn try_wait(&mut self) -> io::Result<Option<ExitStatus>> {
        unsupported()
    }
}
