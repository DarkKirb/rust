//! Horizon-specific definitions


#![stable(feature = "raw_ext", since = "1.1.0")]

pub mod raw;
pub mod fs;


// I have no idea where else to put this but

#[no_mangle]
extern "C" fn __app_init() {
    use ctru_sys::*;
    unsafe {
        srvInit();
        aptInit();
        hidInit();
        fsInit();
        sdmcInit();
        sslcInit(0);
    }
}

#[no_mangle]
extern "C" fn __app_exit() {
    use ctru_sys::*;
    unsafe {
        sslcExit();
        sdmcExit();
        fsExit();
        hidExit();
        aptExit();
        srvExit();
    }
}
